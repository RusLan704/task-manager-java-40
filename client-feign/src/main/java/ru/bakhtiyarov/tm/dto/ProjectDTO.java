package ru.bakhtiyarov.tm.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.bakhtiyarov.tm.enumeration.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDTO extends AbstractEntityDTO implements Serializable {

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private String userId;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    public ProjectDTO(@NotNull String name, @Nullable String description, @Nullable String userId) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }
    
    @NotNull
    public ProjectDTO(@NotNull String name, @Nullable String userId) {
        this.name = name;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
