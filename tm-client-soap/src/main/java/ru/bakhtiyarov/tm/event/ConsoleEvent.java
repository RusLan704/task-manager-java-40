package ru.bakhtiyarov.tm.event;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class ConsoleEvent {

    @NotNull
    private String name = "";

    @NotNull
    public ConsoleEvent(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

}
