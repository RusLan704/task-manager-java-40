package ru.bakhtiyarov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectSoapEndpointService;
import ru.bakhtiyarov.tm.endpoint.soap.TaskSoapEndpoint;
import ru.bakhtiyarov.tm.endpoint.soap.TaskSoapEndpointService;

@Configuration
@ComponentScan("ru.bakhtiyarov.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public TaskSoapEndpointService taskEndpointService() {
        return new TaskSoapEndpointService();
    }

    @Bean
    @NotNull
    public TaskSoapEndpoint taskEndpoint(
            @NotNull @Autowired final TaskSoapEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskSoapEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectSoapEndpointService projectEndpointService() {
        return new ProjectSoapEndpointService();
    }

    @Bean
    @NotNull
    public ProjectSoapEndpoint projectEndpoint(
            @NotNull @Autowired final ProjectSoapEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectSoapEndpointPort();
    }

}
