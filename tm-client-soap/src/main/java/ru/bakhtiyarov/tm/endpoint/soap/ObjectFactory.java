
package ru.bakhtiyarov.tm.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.bakhtiyarov.tm.endpoint.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateProject_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "createProject");
    private final static QName _CreateProjectResponse_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "createProjectResponse");
    private final static QName _FindAllProjects_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "findAllProjects");
    private final static QName _FindAllProjectsResponse_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "findAllProjectsResponse");
    private final static QName _FindProjectById_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "findProjectById");
    private final static QName _FindProjectByIdResponse_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "findProjectByIdResponse");
    private final static QName _RemoveProjectOneById_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "removeProjectOneById");
    private final static QName _RemoveProjectOneByIdResponse_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "removeProjectOneByIdResponse");
    private final static QName _UpdateProjectById_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "updateProjectById");
    private final static QName _UpdateProjectByIdResponse_QNAME = new QName("http://soap.endpoint.tm.bakhtiyarov.ru/", "updateProjectByIdResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.bakhtiyarov.tm.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateProject }
     * 
     */
    public CreateProject createCreateProject() {
        return new CreateProject();
    }

    /**
     * Create an instance of {@link CreateProjectResponse }
     * 
     */
    public CreateProjectResponse createCreateProjectResponse() {
        return new CreateProjectResponse();
    }

    /**
     * Create an instance of {@link FindAllProjects }
     * 
     */
    public FindAllProjects createFindAllProjects() {
        return new FindAllProjects();
    }

    /**
     * Create an instance of {@link FindAllProjectsResponse }
     * 
     */
    public FindAllProjectsResponse createFindAllProjectsResponse() {
        return new FindAllProjectsResponse();
    }

    /**
     * Create an instance of {@link FindProjectById }
     * 
     */
    public FindProjectById createFindProjectById() {
        return new FindProjectById();
    }

    /**
     * Create an instance of {@link FindProjectByIdResponse }
     * 
     */
    public FindProjectByIdResponse createFindProjectByIdResponse() {
        return new FindProjectByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectOneById }
     * 
     */
    public RemoveProjectOneById createRemoveProjectOneById() {
        return new RemoveProjectOneById();
    }

    /**
     * Create an instance of {@link RemoveProjectOneByIdResponse }
     * 
     */
    public RemoveProjectOneByIdResponse createRemoveProjectOneByIdResponse() {
        return new RemoveProjectOneByIdResponse();
    }

    /**
     * Create an instance of {@link UpdateProjectById }
     * 
     */
    public UpdateProjectById createUpdateProjectById() {
        return new UpdateProjectById();
    }

    /**
     * Create an instance of {@link UpdateProjectByIdResponse }
     * 
     */
    public UpdateProjectByIdResponse createUpdateProjectByIdResponse() {
        return new UpdateProjectByIdResponse();
    }

    /**
     * Create an instance of {@link ProjectDTO }
     * 
     */
    public ProjectDTO createProjectDTO() {
        return new ProjectDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "createProject")
    public JAXBElement<CreateProject> createCreateProject(CreateProject value) {
        return new JAXBElement<CreateProject>(_CreateProject_QNAME, CreateProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "createProjectResponse")
    public JAXBElement<CreateProjectResponse> createCreateProjectResponse(CreateProjectResponse value) {
        return new JAXBElement<CreateProjectResponse>(_CreateProjectResponse_QNAME, CreateProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllProjects }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "findAllProjects")
    public JAXBElement<FindAllProjects> createFindAllProjects(FindAllProjects value) {
        return new JAXBElement<FindAllProjects>(_FindAllProjects_QNAME, FindAllProjects.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllProjectsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "findAllProjectsResponse")
    public JAXBElement<FindAllProjectsResponse> createFindAllProjectsResponse(FindAllProjectsResponse value) {
        return new JAXBElement<FindAllProjectsResponse>(_FindAllProjectsResponse_QNAME, FindAllProjectsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "findProjectById")
    public JAXBElement<FindProjectById> createFindProjectById(FindProjectById value) {
        return new JAXBElement<FindProjectById>(_FindProjectById_QNAME, FindProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "findProjectByIdResponse")
    public JAXBElement<FindProjectByIdResponse> createFindProjectByIdResponse(FindProjectByIdResponse value) {
        return new JAXBElement<FindProjectByIdResponse>(_FindProjectByIdResponse_QNAME, FindProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectOneById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "removeProjectOneById")
    public JAXBElement<RemoveProjectOneById> createRemoveProjectOneById(RemoveProjectOneById value) {
        return new JAXBElement<RemoveProjectOneById>(_RemoveProjectOneById_QNAME, RemoveProjectOneById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectOneByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "removeProjectOneByIdResponse")
    public JAXBElement<RemoveProjectOneByIdResponse> createRemoveProjectOneByIdResponse(RemoveProjectOneByIdResponse value) {
        return new JAXBElement<RemoveProjectOneByIdResponse>(_RemoveProjectOneByIdResponse_QNAME, RemoveProjectOneByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "updateProjectById")
    public JAXBElement<UpdateProjectById> createUpdateProjectById(UpdateProjectById value) {
        return new JAXBElement<UpdateProjectById>(_UpdateProjectById_QNAME, UpdateProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.bakhtiyarov.ru/", name = "updateProjectByIdResponse")
    public JAXBElement<UpdateProjectByIdResponse> createUpdateProjectByIdResponse(UpdateProjectByIdResponse value) {
        return new JAXBElement<UpdateProjectByIdResponse>(_UpdateProjectByIdResponse_QNAME, UpdateProjectByIdResponse.class, null, value);
    }

}
