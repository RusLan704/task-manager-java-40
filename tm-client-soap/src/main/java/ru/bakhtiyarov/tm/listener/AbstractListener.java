package ru.bakhtiyarov.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.event.ConsoleEvent;

@NoArgsConstructor
public abstract class AbstractListener {

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void handler(final ConsoleEvent event) throws Exception;

}
