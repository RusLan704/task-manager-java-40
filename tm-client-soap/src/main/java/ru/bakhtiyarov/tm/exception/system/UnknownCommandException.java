package ru.bakhtiyarov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    @NotNull
    public UnknownCommandException(@NotNull final String arg) {
        super("Error! Command ``" + arg + "`` doesn't exist...");
    }

    @NotNull
    public UnknownCommandException() {
        super("Error! Command doesn't exist...");
    }

}