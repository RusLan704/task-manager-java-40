package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    void addAll(@Nullable List<E> tasks);

    @Nullable
    E save(@Nullable E record);

}
