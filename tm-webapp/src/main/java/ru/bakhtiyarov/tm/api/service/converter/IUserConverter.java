package ru.bakhtiyarov.tm.api.service.converter;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.entity.User;

public interface IUserConverter {

    @Nullable
    UserDTO toDTO(@Nullable User user);

    @Nullable
    User toEntity(@Nullable UserDTO userDTO);

}
