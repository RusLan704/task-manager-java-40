package ru.bakhtiyarov.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.service.converter.TaskConverter;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/rest/task")
public class TaskRestController {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final TaskConverter taskConverter;

    @NotNull
    @Autowired
    public TaskRestController(@NotNull final ITaskService taskService, @NotNull TaskConverter taskConverter) {
        this.taskService = taskService;
        this.taskConverter = taskConverter;
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public void create(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) {
        taskService.create(taskDTO.getProjectId(), taskConverter.toEntity(taskDTO));
    }

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> findAll() {
        List<Task> tasks = taskService.findAll();
        return tasks
                .stream()
                .map(taskConverter::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO findById(
            @PathVariable("id") @Nullable final String id
    ) {
        return taskConverter.toDTO(taskService.findById(id));
    }

    @PutMapping(value = "/updateById", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO updateTaskById(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) {
        Task task = taskService.updateTaskById(
                taskDTO.getId(),
                taskDTO.getName(),
                taskDTO.getDescription(),
                taskDTO.getStatus(),
                taskDTO.getStartDate(),
                taskDTO.getFinishDate()
        );
        return taskConverter.toDTO(task);
    }

    @DeleteMapping(value = "/removeById/{id}")
    public void removeOneById(
            @PathVariable("id") @Nullable final String id
    ) {
        taskService.removeOneById(id);
    }

}
