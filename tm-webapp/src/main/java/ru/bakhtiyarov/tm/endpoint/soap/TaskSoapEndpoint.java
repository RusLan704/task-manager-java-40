package ru.bakhtiyarov.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.converter.ITaskConverter;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

//http://localhost:8080/ws/TaskEndpoint?wsdl

@Component
@WebService
public class TaskSoapEndpoint {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final ITaskConverter taskConverter;

    @NotNull
    @Autowired
    public TaskSoapEndpoint(
            @NotNull final ITaskService taskService,
            @NotNull final  ITaskConverter taskConverter
    ) {
        this.taskService = taskService;
        this.taskConverter = taskConverter;
    }

    @WebMethod
    public void createTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO
    ) {
        taskService.create(taskDTO.getProjectId(),taskConverter.toEntity(taskDTO));
    }

    @NotNull
    @WebMethod
    public List<TaskDTO> findAllTasks() {
        @NotNull final List<Task> tasks = taskService.findAll();
        return tasks
                .stream()
                .map(taskConverter::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        return taskConverter.toDTO(taskService.findById(id));
    }

    @Nullable
    @WebMethod
    public TaskDTO updateTaskById(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO
    ) {
        @Nullable Task task = taskService.updateTaskById(
                taskDTO.getId(),
                taskDTO.getName(),
                taskDTO.getDescription(),
                taskDTO.getStatus(),
                taskDTO.getStartDate(),
                taskDTO.getFinishDate()
        );
        return taskConverter.toDTO(task);
    }

    @WebMethod
    public void removeTaskOneById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        taskService.removeOneById(id);
    }
    
}
