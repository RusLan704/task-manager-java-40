<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/_header.jsp"/>


   <h1>PROJECT EDIT</h1>

        <form:form method="POST" action="/projects/update/${project.id}" modelAttribute="project">
            <p>
                <div style="margin-bottom: 5px;">Name:</div>
                <form:input path="name"/>
            </p>

            <p>
                <div style="margin-bottom: 5px;">Description:</div>
                <form:input path="description"/>
            </p>

            <p>
                <div style="margin-bottom: 5px;">Status:</div>
                     <div>
                         <form:select path="status" >
                             <form:option value="${null}" label="--- // ---" />
                             <form:options items="${statuses}" itemLabel="displayName" />
                         </form:select>
                     </div>
                </div>
            </p>

            <p>
                <div style="margin-bottom: 5px;">DATE BEGIN:</div>
                <div><form:input type="date" path="startDate" /></div>
            </p>
            <p>
                 <div style="margin-bottom: 5px;">DATE FINISH:</div>
                 <div><form:input type="date" path="finishDate" /></div>
            </p>

            <input type="submit" value="Update" class="customButton">
        </form:form>

<jsp:include page="../include/_footer.jsp"/>
